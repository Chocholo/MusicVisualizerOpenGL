#include "Libraries\glew\glew.h"
#include "Libraries\freeglut\freeglut.h"
#include "core\shaders\Shader_loader.hpp"
#include <iostream>

GLuint program;


void renderScene(void)
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glClearColor(1.0, 0.0, 0.0, 1.0);//clear red

									 //use the created program
	glUseProgram(program);

	//draw 3 vertices as triangles
	glDrawArrays(GL_TRIANGLES, 0, 3);

	glutSwapBuffers();
}

void init() {
	glEnable(GL_DEPTH_TEST);

	//load and compile shaders
	Core::Shader_Loader shaderLoader;
	program = shaderLoader.CreateProgram("core\\shaders\\vertex\\Vertex_Shader.glsl",
		"core\\shaders\\fragment\\Fragment_Shader.glsl");

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
}
int main(int argc, char **argv)
{

	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DEPTH | GLUT_DOUBLE | GLUT_RGBA);
	glutInitWindowPosition(500, 500);//optional
	glutInitWindowSize(800, 600); //optional
	glutCreateWindow("Music visualizer");
	
	glewInit();
	if (glewIsSupported("GL_VERSION_4_5")) 
	{
		std::cout << "Glew supported";
	}
	else {
		std::cout << "chuj";
	}
	init();
	
	glutDisplayFunc(renderScene);
	glutMainLoop();

	return 0;
}